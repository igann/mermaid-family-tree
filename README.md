```mermaid

graph LR

subgraph Gan
    IsraelsFatherGan[Mейр Gan <br>tbd-tbd]
    IsraelsFathersWife[??? Altshuller <br>tbd-tbd]
end
IsraelsFathersWife --> IsraelGan
IsraelsFathersWife --> NaximGan[Naxim Gan<br>tbd-tbd]
IsraelsFathersWife --> ChaimGan[Chaim Gan<br>tbd-tbd]

subgraph Gan
    NaximGan[Naxim Gan<br>tbd-tbd]
    NaximGanWife[M Ефим<br>tbd-tbd]
end
NaximGanWife --> TysiaGan 
NaximGanWife --> MaraGan[Mara/Мэра Gan<br>tbd-tbd] 
NaximGanWife --> IdaGan[Ida Gan<br>tbd-tbd] 
NaximGanWife --> SonyaGan[Sonya Gan<br>tbd-tbd] 
NaximGanWife --> LeibaGan[Leiba  Gan<br>tbd-tbd] 

subgraph ???
    TysiaGanHusband[???<br>tbd-tbd]
    TysiaGan[Tyuba Gan<br>tbd-tbd]
end
TysiaGan --> Inessa
TysiaGan --> Nona
TysiaGan --> Nella
TysiaGan --> Elvira

Inessa --> Larissa
Larissa --> MaximeVolokitine[Maxime Volokitine]

subgraph Volokitine
    Larissa 
    Volokitine[??? Volokitine]
end


subgraph Gan
    IsraelGan[Israel Gan<br>tbd-tbd]
    Stycia[Stycia Abramovna<br>tbd-tbd]
end
Stycia --> YudifGan
Stycia --> AbramGan

subgraph Rozinov
    Rozinov[??? Rozinov<br>?-?]
    YudifGan[Yudif/Julia Gan <br>tbd-tbd]
end
YudifGan --> BellaRozinova

subgraph Stavinsky
    Stavinsky[??? Stavchansky<br>?-?]
    BellaRozinova[Bella Rozinova<br>tbd-present]
end
BellaRozinova --> RomanStavinsky[Roman Stavchansky<br>tbd-present]

subgraph Gan
    AbramGan[Abram Gan<br>tbd-tbd]
    TamaraKisina[Tamara Kisina<br>xxxx] 
end
TamaraKisina --> MikhailGan

subgraph Gan
    GalinaKnopova[Galina Knopova<br>1953-2017] 
    MikhailGan[Mikhail Gan<br>1948-2014]
end
GalinaKnopova --> IakovGan
GalinaKnopova --> MariaGan

subgraph Gan
    IakovGan[Iakov Gan<br>1979-present] 
    DariaVaryagina[Daria Varyagina<br>1981-present]
end
DariaVaryagina --> oliviagan[Olivia Gan<br>2014-present]
DariaVaryagina --> leogan[Leo Gan<br>2011-present]

subgraph Yakushev
    MikhailYakushev[Mikhail Yakushev<br>1979-present]
    MariaGan[Maria Gan<br>1981-present] 
end
MariaGan --> AnnaYakusheva[Anna Yakusheva<br>2014-present]
MariaGan --> SashaYakushev[Sasha Yakushev<br>2011-present]

```